var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

var sendRoles = (channelId) => {
    const userIds = Object.keys(bot.users).filter(key => key !== bot.id);
    const userCount = userIds.length;
    if (userCount < 3) {
        bot.sendMessage({ to: channelId, message: 'Need at least 3 players!'});
        return;
    }

    const murder = Math.floor((Math.random() * userCount)); 
    let sheriff = murder;
    while (sheriff === murder) {
        sheriff = Math.floor((Math.random() * userCount));
    }

    for (let index = 0; index < userCount; index++) {
        if (index === murder) {
            bot.sendMessage({ to: userIds[index], message: 'You are murder!' });
        } else if (index === sheriff) {
            bot.sendMessage({ to: userIds[index], message: 'You are sheriff!' });
        } else {
            bot.sendMessage({ to: userIds[index], message: 'You are innocent!' });
        }
    }
}

// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
       
        args = args.splice(1);
        switch(cmd) {
            // !ping
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
            case 'test': {
                sendRoles(channelID);
            }
            break;
            // Just add any case commands if you want to..
         }
     }
});

